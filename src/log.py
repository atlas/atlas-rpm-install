from cfg import LOGS_DIR
import os
import logging
import sys
from util import die, createDir, START_EPOCH


FMT = '[%(asctime)s] %(levelname)s -- %(message)s'

def init(args):
    """Add handlers to the root logger."""
    # send error and above to the console
    # addConsoleHandler()

    # send all levels to file for future ref
    logname = constructLogFileName(args)
    addFileHandler(logname)


def addFileHandler(logFileName):
    """Add a FileHandler to the root logger."""
    if not createDir(LOGS_DIR):
        m = '{0}: unable to create log dir\n'.format(LOGS_DIR)
        m += str(e)
        die(m)

    logfile = os.path.join(LOGS_DIR, logFileName)
    print('Install log file: {0}'.format(logfile))
    logging.basicConfig(filename=logfile,
                        filemode='w',
                        level=logging.DEBUG,
                        format=FMT)


def addConsoleHandler():
    """Add an error-level StreamHandler to the root logger."""
    sh = logging.StreamHandler(sys.stderr)
    sh.setLevel(logging.ERROR)
    sh.setFormatter(logging.Formatter(FMT))
    logging.getLogger().addHandler(sh)


def constructPathToLogFile(args):
    return os.path.join(LOGS_DIR, constructLogFileName(args))


def constructLogFileName(args):
    """The output log file for install attempt has a name
    that is based on the release we are installing, and the datetime.
    This constructed log name is then used within createLogger().
    """
    name = '__'.join([args.branch,
                      args.platform,
                      args.release,
                      args.datetime,
                      str(START_EPOCH)])
    return '{0}.log'.format(name)


# -- shortcuts
def debug(s, exc_info=False):
    logging.debug(s, exc_info=exc_info)


def info(s, exc_info=False):
    logging.info(s, exc_info=exc_info)


def warning(s, exc_info=False):
    logging.warning(s, exc_info=exc_info)


def error(s, exc_info=False):
    logging.error(s, exc_info=exc_info)


def fatal(s, exc_info=False):
    logging.fatal(s, exc_info=exc_info)
