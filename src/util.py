from collections import namedtuple
from exc import URLRetrieveFail
import functools
import log
import os
import requests
import subprocess
import sys
import time


# The approximate epoch when the install script
# was started running. Module level, so that it
# is constant on imports
START_EPOCH = int(time.time())


def shellCmd(cmd, bufsize=1, shell=True, asText=False):
    """Run a shell command, returning a namedtuple with useful info."""
    Result = namedtuple('Result', 'exitcode ok cmd out err runtime raised')
    start = int(time.time())
    out = []
    err = []
    o = ''
    e = ''
    raised = False
    log.debug('Executing shell cmd:\n{0}'.format(cmd))
    try:
        proc = subprocess.Popen(cmd,
                                bufsize=bufsize,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                shell=shell)

        while proc.poll() is None:
            line = proc.stdout.readline()
            if line and line.strip():
                line = line.strip()
                out.append(line)
                log.debug(line)

        o, e = proc.communicate()
    except Exception as e:
        raised = True
        log.warning('Shell cmd raised an exception:', exc_info=True)

    duration = int(time.time()) - start
    log.debug('Shell cmd took {0}s'.format(duration))

    o = [oo.strip() for oo in o.split('\n') if oo.strip()]
    e = [ee.strip() for ee in e.split('\n') if ee.strip()]
    out.extend(o)
    err.extend(e)

    if asText:
        out = '\n'.join(out)
        err = '\n'.join(err)
    else:
        out = [o.strip() for o in out if o.strip()]
        err = [e.strip() for e in err if e.strip()]

    ec = proc.returncode
    return Result(ec, ec == 0, cmd, out, err, duration, raised)


def timeit(func):
    @functools.wraps(func)
    def wrapped(*args, **kws):
        fname = func.__name__
        log.debug('{0}: begin function'.format(fname))
        start = int(time.time())
        val = func(*args, **kws)
        spent = int(time.time()) - start
        log.debug('{0}: end function [took {1}s]'.format(fname, spent))
        return val
    return wrapped


def getURL(url, timeout=None):
    try:
        response = requests.get(url, timeout=timeout)
    except requests.exceptions.Timeout:
        m = 'Timed out after {0}s trying to reach {1}'.format(timeout, url)
        log.warning(m)
        raise
    else:
        log.info(response.text)
        if not response.ok:
            m = 'Attempt to retrieve URL failed with '
            m += 'HTTP code {0}'.format(response.status_code)
            log.warning(m)
            raise URLRetrieveFail(m)
    return response


def deleteFile(path):
    try:
        os.remove(path)
    except:
        pass
    return not os.path.exists(path)


def createFile(path, contents='', overwrite=False):
    if os.path.exists(path) and not overwrite:
        return
    try:
        with open(path, 'w') as f:
            f.write(contents + '\n')
    except:
        pass

    return os.path.exists(path)


def createDir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as e:
            pass

    return os.path.exists(path)


def die(message, exitcode=1):
    sys.stderr.write(message + '\n')
    sys.stderr.flush()
    sys.exit(exitcode)


def sendEmail(subject, body, *recipients):
    MAILX_CMD = '/bin/mailx'
    addrs = ' '.join(recipients)
    cmd = 'echo "{0}" | {1} -s "{2}" {3}'
    cmd = cmd.format(body, MAILX_CMD, subject, addrs)
    result = shellCmd(cmd)
    if result.ok:
        log.info('Email sent to {0}'.format(addrs))
    else:
        log.warning('Unable to send email to {0}'.format(addrs))
        if result.out:
            for o in result.out:
                log.warning(o)
        if result.err:
            for o in result.err:
                log.warning(o)
