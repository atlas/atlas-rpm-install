"""
Simple application monitor.

At various points in the execution, the application calls one of
the below methods. This updates the in-memory status dict, and then
writes it out as JSON to a file in cfg.MONITOR_DIR called:
<branch>__<platform>__<release>__<buildStart>__<installStart>.json
This JSON file can then be used by a 3rd-party script to display
the state of this and previous installations (this application
provides one itself: status.py).
Format of the status dict is (epoch = integer Unix epoch):
{
    'process': {
        'start': epoch,
        'stop': (None | epoch),
        'states': [{}, {}, ...]
    },
    'install': [{}, {}, ....],
    'timings': [{}, {}, ....]
}
The process/states list contains a dict per state traversed,
and in the order traversed. For example:
{
    'name': 'run',
    'when': epoch
}
or
{
    'name': 'err',
    'when': epoch,
    'info': 'Nightly already installed'
}
The install and timings list of dicts have a length equal
to the number of installs. In general will be 1, but for
full releases where we split into offline and hlt, the length
will be 2. The install dicts are of the form:
{
    'start': epoch,
    'stop': (epoch|None),
    'status': (run|fail|ok),
    'info': ('' | <failReason>)
    'rpms': [list of rpms installed]
}
The timings dicts are of the form:
{
    'install': [intNumbSeconds1, intNumbSeconds2, ..],
    'publish': (None | intNumbSeconds)
}
where timings/install is a list in the same order as the
install key list of dicts.
"""
import cfg
import cli
import json
import log
import os
import time
from util import createDir, createFile, die, START_EPOCH


def createMonFileName():
    args = cli.parse(validate=False)
    monfile = '__'.join([args.branch,
                         args.platform,
                         args.release,
                         args.datetime,
                         str(START_EPOCH)])
    return monfile + '.json'


class Status(object):
    status = {
        'process': {
            'states':[]
        },
        'install': [],
        'timings': {'install':[], 'publish': None}
    }

    def __init__(self):
        self.createMonBaseDir()
        fname = createMonFileName()
        self.statusfile = os.path.join(cfg.MONITOR_DIR, fname)

    def createMonBaseDir(self):
        mondir = cfg.MONITOR_DIR
        if not os.path.exists(mondir):
            if not createDir(mondir):
                # yikes, no monitoring base dir
                m = '{0}: unable to create monitoring dir'.format(mondir)
                die(m)

    def write(self):
        with open(self.statusfile, 'w') as f:
            f.write(json.dumps(self.status))


class State:
    def __init__(self, name, when=None, info=None):
        self.name = name
        self.when = int(time.time()) if when is None else when
        self.info = '' if info is None else info


class Process(Status):
    def __init__(self):
        Status.__init__(self)

    def waiting(self, contents):
        state = State('pend', info=contents)
        self.status['process']['states'].append(state.__dict__)
        self.write()

    def ongoing(self):
        state = State('run')
        self.status['process']['states'].append(state.__dict__)
        self.write()

    def error(self, reason=''):
        state = State('err', info=reason)
        self.status['process']['states'].append(state.__dict__)
        self.write()

    def start(self):
        self.status['process']['start'] = START_EPOCH
        self.write()

    def stop(self):
        self.status['process']['stop'] = int(time.time())
        self.write()

process = Process()

class Install(Status):
    def __init__(self):
        Status.__init__(self)

    def start(self, rpms):
        self.status['install'].append({
            'start': int(time.time()),
            'rpms': rpms,
            'stop': None,
            'state': 'run'
        })
        self.write()

    def failed(self, reason=''):
        d = self.status['install'][-1]
        d['stop'] = int(time.time())
        d['state'] = 'fail'
        d['info'] = reason
        self.status['install'][-1] = d
        self.write()

    def succeeded(self):
        d = self.status['install'][-1]
        d['stop'] = int(time.time())
        d['state'] = 'ok'
        self.status['install'][-1] = d
        self.write()

install = Install()


class Timings(Status):
    def __init__(self):
        Status.__init__(self)

    def install(self, seconds):
        self.status['timings']['install'].append(seconds)
        self.write()

    def publish(self, seconds):
        self.status['timings']['publish'] = seconds
        self.write()

timings = Timings()
