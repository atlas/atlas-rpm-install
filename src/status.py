#! /usr/bin/env python

"""status.py - report on the status of installations.

!!!!! NOTE: THIS SCRIPT IS CURRENTLY BROKEN, PLEASE DO NOT USE !!!!!

"""

import argparse
from cfg import MONITOR_DIR
from collections import namedtuple, defaultdict
import datetime
import operator
import os
import re
import time


_patt = (
    '(.*?)__'  # branch
    '(.*?)__'  # platform
    '(rel_\d)__'  # release
    '(\d{4}-\d\d-\d\dT\d\d\d\d)__'  # nicos build start date/time
    '(\d{10})'  # epoch of install process start time
)
MON_DIR_REGEX = re.compile(_patt)
MON_DIR_TOKENS = ('branch', 'platform', 'release', 'buildStart', 'epoch')

NOW = datetime.datetime.now()

# --------------------------------------------------------------------
# - Main actors ------------------------------------------------------
# --------------------------------------------------------------------

class MonDir(object):
    """Represents a monitoring directory, which has a name of the form
    as defined by MON_DIR_REGEX.
    """
    def __init__(self, path):
        self.path = path
        self.name = os.path.basename(path)
        self._files = {}

    @property
    def files(self):
        if not self._files:
            for f in os.listdir(self.path):
                self._files[f] = os.path.join(self.path, f)
        return self._files

    def __getitem__(self, fname):
        return self.files.get(fname)


def memoize(func):
    cache = {}
    def wrapped(a):
        if a not in cache:
            cache[a] = func(a)
        return cache[a]
    return wrapped


class Run(object):
    def __init__(self, mondir):
        self.mondir = mondir
        self._parseMonDirName(mondir.name)

    def _parseMonDirName(self, name):
        tokens = MON_DIR_REGEX.match(name).groups()
        self.__dict__.update(dict(zip(MON_DIR_TOKENS, tokens)))
        self.nightly = os.path.join(self.branch, self.platform)

        # transform 2016-08-23T2334 to 2016-08-23 23:34
        bs = self.buildStart
        bs = bs.replace('T', ' ')
        bs = bs[:-2] + ':' + bs[-2:]
        self.buildStart = bs

    def colnames(self):
        return ['Status',
                'Release',
                'Nightly',
                'Install Start',
                'Install Done',
                'Build Start',
                'Info']

    @memoize
    def summary(self):
        state, info = self.status
        return (
            state,
            self.release,
            self.nightly,
            epochToStr(self.start),
            epochToStr(self.stop),
            self.buildStart,
            info
        )

    def _epochInFile(self, fname, default=None):
        contents = fileContents(self.mondir[fname])
        try:
            return int(contents)
        except:
            return contents if not default else default

    def done(self):
        return self.stop != 'n/a'

    @property
    def start(self):
        return self._epochInFile('process.start', default='n/a')

    @property
    def stop(self):
        return self._epochInFile('process.stop', default='n/a')

    @property
    def status(self):
        return self._statusDone() if self.done() else self._statusNotDone()

    def _statusDone(self):
        state = info = ''
        if self.succeeded():
            state = 'OK'
            install, publish = self.timings()
            i = toMinsSecs(install)
            p = toMinsSecs(publish)
            info = 'Install: {0}, publish: {1}'.format(i, p)
        elif self.failed():
            state = 'FAIL'
            _, info = self.fail()
        else:
            state = 'ERR'
            info = self.error()
            info = 'Install script crash?' if not info else info
        return (state, info)

    def _statusNotDone(self):
        state = info = ''
        if self.running():
            state = 'RUN'
        else:
            state = 'PEND'
            info = self.waiting()[1]
        return (state, info)

    def running(self):
        return self._epochInFile('process.ongoing')

    def waiting(self):
        contents = fileContents(self.mondir['process.waiting'])
        if contents:
            toks = contents.split()
            epoch, info = toks[0], ' '.join(toks[1:])
            return epoch, info

    def error(self):
        contents = fileContents(self.mondir['process.error'])
        if contents:
            toks = contents.split()
            info = ' '.join(toks[1:])
            return info

    def succeeded(self):
        """Did the install succeed?"""
        return self.mondir['install.ok']

    def failed(self):
        """Did the install fail?"""
        failFile = self.mondir['install.fail']
        return failFile is not None and os.path.exists(failFile)

    def fail(self):
        contents = fileContents(self.mondir['install.fail'])
        if contents:
            toks = contents.split()
            epoch, info = toks[0], ' '.join(toks[1:])
            return epoch, info

    def timings(self):
        installTime = self._epochInFile('timings.install')
        publishTime = self._epochInFile('timings.publish')
        return (installTime, publishTime)


# --------------------------------------------------------------------
# - Formatters -------------------------------------------------------
# --------------------------------------------------------------------

class Text(object):
    def __init__(self, runs):
        self.runs = runs

    def __str__(self):
        # same for each run, so just take the first one
        colnames = self.runs[0].colnames()
        # get the numb chars in each field of the run.summary() tuple
        widths = [[len(f) for f in run.summary()] for run in self.runs]
        # find across runs the max width of each field
        maxWidths = [max(width) for width in zip(*widths)]

        txt = []
        for i, v in enumerate(maxWidths):
            colNameSize = len(colnames[i])
            v = v if int(v) >= colNameSize else colNameSize
            txt.append('{%i:<%s}' % (i, v))

        fstr = ' | '.join(txt)

        o = ['-' * sum(maxWidths)]
        o.append(fstr.format(*colnames))
        o.append('-' * sum(maxWidths))
        for run in self.runs:
            o.append(fstr.format(*run.summary()))
        return '\n'.join(o)


class HTML(object):
    def __init__(self, runs):
        self.runs = runs

    def _tag(self, name, content):
        return '<{0}>{1}</{0}>\n'.format(name, content)

    def _title(self):
        return 'Status: ATLAS CVMFS Nightly Installs'

    def _css(self):
        return """
        html,body{margin:0;padding:0;border:0}
        body{
            font-family:Century Gothic,CenturyGothic,AppleGothic,sans-serif;
            background-color:#eee;
        }
        .active{border:2px black solid}
        .box{
            margin:3px;
            width:120px;
            height:50px;
            overflow:hidden;
            border-radius:10px;
        }
        .fail{background-color:#CD5C5C;}
        .ok{background-color:#8FBC8F;}
        .err{background-color:yellow;}
        .run{background-color:white;}
        .pend{background-color:white;}
        .nightly{
            font-size:1.2em;
            background-color:#ccc;
            padding:5px;
            border-radius:10px;
            white-space:nowrap;
        }
        .legend td:first-child{
            font-style:italic;
        }
        .legend td:not(:first-child) {
            font-size:1.2em;
            background-color:#ccc;
            border-radius:10px;
            padding:15px;
            text-align:center;
            width:120px;
        }
        .empty{background-color:#ddd;opacity:0.5;}
        .overview{font-size:1.0em;margin:8px;}
        .subtableDiv{display:none;background-color:#ddd;border-radius:5px}
        .subtable{display:none;background-color:#ddd;border-radius:5px}
        .subtable td{font-size:0.8em;padding:5px;vertical-align:top}
        .subheader td{border-bottom:1px black dotted;}
        table{padding:10px}
        #titlebar{
            top:0;
            width:100%;
            height:50px;
            white-space:nowrap;
            border-bottom:7px #4682B4 solid;
            background-color:#eee;
            color:#333;
            padding:5px;
            font-family:arial;
            font-size:2.5em;
        }
        @keyframes highlight{
             from{background-color:#eee;}
             to{background-color:yellow;}
        }
        #help{
            animation-name:highlight;
            animation-duration:0.5s;
            animation-iteration-count: 2;
            animation-direction: alternate;
        }
        """

    def _jsImport(self):
        return '<script src="zepto.min.js"></script>'

    def __str__(self):
        n = self._tag
        style = n('style', self._css())
        title = n('title', self._title())
        jslib = self._jsImport()
        head = n('head', title + style + jslib)
        bodyContent = ('<div id="titlebar">'
                       'Status of the ATLAS Nightly CVMFS Installs'
                       '</div>')
        bodyContent += self._table()
        bodyContent += self._js()
        body = n('body', bodyContent)
        page = n('html', head + body)
        return '<!doctype html>' + page

    def _js(self):
        return """
        <script>
        $(".box").click(function(){
           var tgt = $(this).attr("id") + "_tgt";
           $("#" + tgt).toggle();
           $(this).hasClass('active')? $(this).removeClass('active'): $(this).addClass('active');
        });
        </script>
        """

    def _tableRow(self, nightly, runs):
        t = []
        t.append('<tr>')
        t.append('<td class="nightly">{0}</td>'.format(nightly))

        runs = removeDuplicates(runs)

        dayStart = epochStartToday()
        byDaysAgo = defaultdict(list)
        for run in runs:
            buildStart = nicosDatetimeToEpoch(run.buildStart)
            daysAgo = 0 if buildStart >= dayStart else ((dayStart - buildStart) / (24 *3600)) + 1
            byDaysAgo[daysAgo].append(run)

        childrows = []

        missingDeps = re.compile('Missing dep.*?\((.*?)\)')
        for index in reversed(range(7)):
            runs = byDaysAgo.get(index, [])
            try:
                run = runs[0]
                state, release, _, start, stop, buildStart, info = run.summary()

                nightlyID = nightly.replace('/', '_').replace('.', '_').replace('-', '_')
                parentID = 'src_{0}_{1}'.format(nightlyID, release)
                childID = '{0}_tgt'.format(parentID)

                td = '<td class="box {0}" id="{1}">'.format(state.lower(), parentID)
                td += '<div class="overview">{0}: {1}</div>'.format(release, state)
                td += '</td>'.format(buildStart)
                t.append(td)

                nicosURL = 'http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=g&'
                nicosURL += 'nightly={0}&rel={1}&ar={2}'.format(run.branch, release, run.platform)
                repoURL = 'https://cern.ch/atlas-software-dist-eos/RPMs/nightlies/'
                repoURL = os.path.join(repoURL, run.branch, run.platform, release)

                childrow = '<table id="{0}" class="subtable">'.format(childID)
                childrow += '<tr class="subheader">'
                childrow += '<td rowspan="2" style="padding:15px;background-color:#ccc;border-top-left-radius:5px;border-bottom-left-radius:5px;border-bottom:none;border-right:1px black dotted;">{0}<br>'.format(release)
                childrow += '<a href="{0}">NICOS</a> | '.format(nicosURL)
                childrow += '<a href="{0}">Repo</a>'.format(repoURL)
                childrow += '</td>'
                childrow += '<td>Build Start</td><td>Install Start</td><td>Install Done</td><td>Info</td>'
                childrow += '</tr>'
                childrow += '<tr>'
                childrow += '<td>{0}</td>'.format(buildStart)
                childrow += '<td>{0}</td>'.format(start)
                childrow += '<td>{0}</td>'.format(stop)

                match = missingDeps.match(info)
                if match:
                    deps = match.groups()[0].split()
                    info = 'Missing Dependencies:<br>{0}'.format('<br>'.join(deps))
                childrow += '<td>{0}</td>'.format(info)
                childrow += '</tr>'
                childrow += '</table>'
                childrows.append(childrow)
            except IndexError:
                t.append('<td class="box empty"></td>')

        t.append('</tr>')
        for cr in childrows:
            t.append('<tr><td></td><td colspan="7">' + cr + '</td></tr>')

        return '\n'.join(t)

    def _legendRow(self):
        t = []
        t.append('<tr class="legend"><td id="help">(Click a rel_X box for more info)</td>')
        for i in reversed(range(7)):
            if i == 0:
                word = 'Today'
            elif i == 1:
                word = 'Yesterday'
            else:
                word = '{0} days ago'.format(i)

            t.append('<td>{0}</td>'.format(word))
            # t.append('<div class="">{0}</div>'.format(word))
        t.append('</tr>')
        return '\n'.join(t)


    def _table(self):
        t = []
        grouped = defaultdict(list)
        for r in self.runs:
            grouped[r.nightly].append(r)

        t.append('<table>')
        t.append(self._legendRow())
        for nightly in sorted(grouped):
            row = self._tableRow(nightly, grouped[nightly])
            t.append(row)

        t.append('</table>')
        return '\n'.join(t)


class JSON(object):
    def __init__(self, runs):
        self.runs = runs

    def __str__(self):
        import json
        cols = self.runs[0].colnames()
        d = {'runs': [dict(zip(cols, run.summary())) for run in self.runs]}
        return json.dumps(d)


# --------------------------------------------------------------------
# - Helper functions -------------------------------------------------
# --------------------------------------------------------------------

def epochStartToday():
    """Get the epoch integer seconds corresponding to the start of
    the NICOS day (defined as 21:00)
    """
    startDayHour = 21  # nicos midnight is 9pm
    if NOW.hour >= startDayHour:
        midnight = startDayHour
        offset = 0
    else:
        midnight = 0
        offset = 24 - startDayHour

    return int(time.time()) - secondsSinceMidnight(midnight) - (offset * 3600)


def secondsSinceMidnight(midnightHour=0):
    diffHours = NOW.hour - midnightHour
    diffMins = NOW.minute
    diffSecs = NOW.second
    totdiff = (diffHours * 3600) + (diffMins * 60) + diffSecs
    return totdiff


def nicosDatetimeToEpoch(dt):
    """Convert the NICOS datetime string (2016-08-23 21:34)
    to an epoch integer seconds.
    """
    d, t = dt.split()
    year, month, day = [int(i) for i in d.split('-')]
    hour, mins = [int(i) for i in t.split(':')]
    dt = datetime.datetime(year, month, day, hour, mins)
    return int(time.mktime(dt.timetuple()))


def removeDuplicates(runs):
    """Remove runs with identical NICOS build start datetimes,
    returning only the youngest run (according to install start time).
    Duplicates may arise if we try more than once to install
    a given release (perhaps the first time there's a server
    timeout, etc).
    """
    byRelease = defaultdict(list)
    for run in runs:
        byRelease[run.release].append((run.start, run))

    runs = []
    for release, installs in byRelease.items():
        for runstart, run in reversed(sorted(installs)):
            info = run.status[1]
            # If we accidentally try and install the same nightly
            # again, the corresponding monitor dir will be the
            # most recent one and thus will be shown, when in fact
            # the more interesting/appropriate result to show is
            # the actual install attempt one
            if 'Already installed' in info:
                continue
            runs.append(run)
            break
    return runs


def getAllMonDirs(ndays=7):
    """Retrieve the monitoring directories from the past ndays.
    If ndays is -1, retrieve all.
    """
    now = int(time.time())
    for thing in os.listdir(MONITOR_DIR):
        match = MON_DIR_REGEX.match(thing)
        if match:
            if ndays != -1:
                installEpoch = int(thing.split('__')[-1])
                secondsSinceInstall = now - installEpoch
                oldest = now - (ndays * 24 * 3600)
                if installEpoch < oldest:
                    continue

            path = os.path.join(MONITOR_DIR, thing)
            yield MonDir(path)


def fileContents(fpath):
    try:
        with open(fpath) as f:
            return f.read().strip()
    except:
        return None


def epochToStr(epoch):
    try:
        lt = time.localtime(float(epoch))
    except:
        return epoch
    else:
        s = time.strftime('%Y-%m-%d %H:%M', lt)
        return s


def toMinsSecs(secs):
    mins, secs = divmod(int(secs), 60)
    secs = secs if secs >= 10 else ('0' + str(secs))
    return '{0}m{1}s'.format(mins, secs)


def parseArgs():
    parser = argparse.ArgumentParser('Get the status of past/current installs')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--text', action='store_true', help='Output as text')
    group.add_argument('--html', action='store_true', help='Output as HTML')
    group.add_argument('--json', action='store_true', help='Output as JSON')

    help = 'Show all entries (default: last 7 days)'
    parser.add_argument('--all', action='store_true', help=help, default=False)
    return parser.parse_args()


def getRuns(showAll=False):
    # By default, only retrieve last 7 days
    ndays = 7 if not showAll else -1
    runs = [Run(md) for md in getAllMonDirs(ndays=ndays)]
    runs = [r for r in reversed(sorted(runs, key=operator.attrgetter('start')))]
    return runs


def getFormatter(fmt):
    return (fmt.text and Text) or (fmt.json and JSON) or (fmt.html and HTML)


def main():
    args = parseArgs()
    runs = getRuns(showAll=args.all)
    formatter = getFormatter(args)
    print(formatter(runs))


if __name__ == '__main__':
    main()
