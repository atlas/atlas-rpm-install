import cfg
import cli
import contextlib
from exc import TooManyLocks, LockAcquireFailure, LockAcquireTimeOut
import glob
import log
import monitor
import os
from process import THIS_PID
import time
from util import START_EPOCH, deleteFile, sendEmail


# - Locks and lock requests
LOCK_DIR = cfg.WORK_BASE_DIR
LOCK_FILE_NAME = 'lock.{0}.{1}'.format(START_EPOCH, THIS_PID)
LOCK_FILE = os.path.join(LOCK_DIR, LOCK_FILE_NAME)

LOCK_REQUEST_FILE_NAME = 'lockrequest.{0}.{1}'.format(START_EPOCH, THIS_PID)
LOCK_REQUEST_FILE = os.path.join(LOCK_DIR, LOCK_REQUEST_FILE_NAME)

LOCK_FILE_PATT = os.path.join(LOCK_DIR, 'lock.*.*')
LOCK_REQUEST_FILE_PATT = os.path.join(LOCK_DIR, 'lockrequest.*.*')


class LockRequest:
    def __init__(self, requestfile):
        self.requestfile = requestfile

    def create(self):
        args = cli.parse(validate=False)
        contents = '/'.join([args.branch,
                             args.platform,
                             args.release,
                             args.datetime])
        try:
            with open(self.requestfile, 'w') as f:
                contents = ''
                f.write(contents + '\n')

            m = 'Created lock request OK: {0}'.format(self.requestfile)
            log.info(m)
        except Exception as e:
            m = 'Unable to create lock request: {0}'.format(self.requestfile)
            log.fatal(m, exc_info=True)
            monitor.process.error('Unable to create lock request')
            raise

    def delete(self):
        log.info('Deleting lock request: {0}'.format(self.requestfile))
        for attempt in range(3):
            if deleteFile(self.requestfile):
                break
            time.sleep(5)
        else:
            # first we log the error
            m = 'Unable to remove lock request: {0}'.format(self.lockfile)
            log.error(m)
            log.error('Please remove manually ASAP.')

            # now we send email as this will stop future installs
            subject = 'ERROR: nightly RPM install cannot delete lock request'
            body = """
            The install script with PID {0} was unable to delete
            its lock request file:
                {1}
            Please remove this manually ASAP to allow subsequent
            install processes to continue.
            """.format(THIS_PID, self.requestfile)
            addrs = cfg.EMAIL_ON_FAIL
            sendEmail(subject, body, *addrs)

    def position(self):
        """Where is this request in the queue?"""
        requests = existingRequests(LOCK_REQUEST_FILE_PATT)
        for pos, req in enumerate(requests, 1):
            if req == self.requestfile:
                return pos
        return -1  # this should never happen


def existingLocks(lockpatt):
    return glob.glob(lockpatt)


def existingRequests(requestPatt):
    return sorted(glob.glob(requestPatt))


class Lock:
    def __init__(self, lockfile):
        self.lockfile = lockfile
        self.acquired = False

    def acquire(self):
        """If no lock currently exists i.e no install process ongoing,
        then create one for this process. Otherwise, create a lock request
        file.
        """
        locks = existingLocks(LOCK_FILE_PATT)
        if not locks:
            # No locks exist, but there may be requests queued.
            # If there are no requests either, the lock is ours.
            # If there are requests, and the first one (epoch-sorted)
            # corresponds to THIS_PID, then again the lock is ours.
            # If however the first request is another pid's, then
            # we wait.
            requests = existingRequests(LOCK_REQUEST_FILE_PATT)

            if not requests or requests[0] == LOCK_REQUEST_FILE:
                self.create()
                log.info('Acquired lock OK: {0}'.format(self.lockfile))
                self.acquired = True
            else:
                m = 'Another lock request is first in queue:\n{0}'
                m = m.format(requests[0])
                raise LockAcquireFailure(m)

        else:
            nlocks = len(locks)
            if nlocks > 1:
                m = '{0} lock files found '.format(nlocks)
                m += '(expected 0 or 1)\n'
                m += '{0}'.format('\n'.join(locks))
                log.fatal(m)
                monitor.process.error('Too many locks')
                raise TooManyLocks(m)
            else:
                m = 'Lock already exists: {0}'.format(locks[0])
                log.info(m)
                raise LockAcquireFailure(m)

    def create(self):
        """A lock is a simple text file with name:
            lock.<epoch>.<pid>
        where pid is the install.py process pid that created the lock,
        and epoch is the integer Unix epoch corresponding to the creation
        time of the lock. The file contains the nightly being installed
        by the process, and is of the form:
        22.0.X-VAL/x86_64-slc6-gcc49-opt/rel_2/2016-08-17T1523
        If no lockfile can be output, the process will raise and exit.
        """
        args = cli.parse(validate=False)
        contents = '/'.join([args.branch,
                             args.platform,
                             args.release,
                             args.datetime])
        try:
            with open(self.lockfile, 'w') as lf:
                lf.write(contents + '\n')
        except IOError as e:
            m = 'Unable to create lockfile: {0}'.format(self.lockfile)
            log.fatal(m, exc_info=True)
            monitor.process.error('Unable to create lockfile')
            raise

    def release(self):
        if not self.acquired:
            return

        self.acquired = False
        if not os.path.exists(self.lockfile):
            log.error('Inexistant lock file: {0}\n'.format(self.lockfile))
            m = 'Despite the lock acquired flag being set. '
            m += 'Was it removed manually?'
            log.error(m)
        else:
            log.info('Removing lock: {0}'.format(self.lockfile))
            for attempt in range(3):
                if deleteFile(self.lockfile):
                    break
                time.sleep(5)
            else:
                # first we log the error
                m = 'Unable to remove lock: {0}\n'.format(self.lockfile)
                log.fatal(m)
                log.fatal('Please remove manually ASAP.')

                # now we send email as this will stop future installs
                subject = 'ERROR: nightly RPM install cannot delete lock'
                body = """
                The install script with PID {0} was unable to delete
                its lock file after installing. Please remove this manually
                ASAP to allow subsequent install processes to continue:
                   {1}
                """.format(THIS_PID, self.lockfile)
                addrs = cfg.EMAIL_ON_FAIL
                sendEmail(subject, body, *addrs)


def lockAcquireTimedOut():
    waiting = int(time.time()) - START_EPOCH
    return cfg.LOCK_ACQUISITION_MAX_WAIT >= 0 \
        and waiting >= cfg.LOCK_ACQUISITION_MAX_WAIT


@contextlib.contextmanager
def lock(lockfile):
    lock_ = Lock(lockfile)

    try:
        lock_.acquire()
        monitor.process.ongoing()
        yield
    except LockAcquireFailure:
        # failed to obtain lock, create a request for it
        log.info('Failed to acquire lock, creating a request')
        request = LockRequest(LOCK_REQUEST_FILE)
        request.create()

        pos = request.position()
        sfx = {1: 'st', 2: 'nd', 3: 'rd'}
        txt = '{0}{1} in queue'.format(pos, sfx.get(pos, 'th'))
        monitor.process.waiting(txt)

        while True:
            if lockAcquireTimedOut():
                request.delete()
                m = 'Failed to acquire lock within max wait time {0}s, exiting'
                m = m.format(cfg.LOCK_ACQUISITION_MAX_WAIT)
                m += ' (max wait time is configurable in cfg.py)'
                log.fatal(m)

                e = 'Failed to acquire lock within {0}s'
                e = e.format(cfg.LOCK_ACQUISITION_MAX_WAIT)
                monitor.process.error(e)
                raise LockAcquireTimeOut(m)

            time.sleep(cfg.SECS_BETWEEN_LOCK_CHECKS)

            try:
                lock_.acquire()
                request.delete()
                monitor.process.ongoing()
                yield
                break
            except LockAcquireFailure:
                m = 'Failed to acquire lock, going to sleep '
                m += 'for {0}s'.format(cfg.SECS_BETWEEN_LOCK_CHECKS)
                log.info(m)

                pos = request.position()
                txt = '{0}{1} in queue'.format(pos, sfx.get(pos, 'th'))
                monitor.process.waiting(txt)
    finally:
        if lock_.acquired:
            lock_.release()
