from collections import namedtuple
from cfg import RPM_REPO_NIGHTLY_BASE
import os
import repos


def test_repos_instance_str():
    name = 'test-repo'
    fullname = 'My test repo'
    url = 'https://fakeurl.com/fake/'
    r = repos.Repo(name, fullname, url)
    expected = """[test-repo]
name=My test repo
baseurl=https://fakeurl.com/fake/
enabled=1
"""
    assert str(r) == expected


def test_nightly_repo_url():
    args = namedtuple('Args', 'branch platform release')
    run = '22.0.X/x86_64-slc6-gcc49-opt/rel_4'
    a = args(*run.split('/'))
    expected = os.path.join(RPM_REPO_NIGHTLY_BASE, run)
    sortedURL = repos.nightlyRepoURL(a, sorted=True)
    unsortedURL = repos.nightlyRepoURL(a, sorted=False)
    assert unsortedURL == expected
    assert sortedURL == expected + '/?C=M;O=A'


def test_parse_rpms_page():
    page = '<a href="ying.rpm">ying</a><a href="yang.text">yang</a>'
    links = repos.parseRPMsPage(page)
    assert type(links) == type([])
    assert len(links) == 1
    assert links[0] == 'ying.rpm'


def test_get_remote_repos():
    args = namedtuple('Args', 'branch platform release datetime')
    run = '22.0.X/x86_64-slc6-gcc49-opt/rel_4'
    a = args(*run.split('/'), datetime='2016-12-25T1234')
    installdir = os.getcwd()  # irrelevant what it actually is
    repos.getRemoteRepos(a, installdir)


def _test_list_nightly_rpms():
    pass
