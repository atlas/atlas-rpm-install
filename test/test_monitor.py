import sys
sys.argv[1:] = [
    '-r',
    '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
    '-n',
    '2016-08-23T1234'
]

import mock
import monitor
import os
import pytest
import util


def test_monitor_file():
    expect = ('22.0.X__x86_64-slc6-gcc49-opt__rel_4'
              '__2016-08-23T1234__'
              '{0}.json'.format(util.START_EPOCH))
    assert os.path.basename(monitor.process.statusfile) == expect
