#!/bin/bash

set -e;

# Install script for the new server
# Download, configure and install local python2.7 version
# Download, install virtualenv version
# Create a virtualenv, activate it and pip install stuff
# Everything gets installed below $BASE dir, change it if you wish

BASE=$HOME;

# -- Functions -------------------------------
fail(){
    message=$1;
    echo -e "\033[1;31;m$message\033[0m";
    exit 1;
}

ok(){
    message=$1;
    echo -e "\033[1;32;m$message\033[0m";
}

installPython(){
    version=$1;
    wget https://www.python.org/ftp/python/$version/Python-$version.tgz;
    tar xvfz Python-$version.tgz;

    cd Python-$version;
    ./configure --prefix=$BASE/.localpython;
    make && make install;
    return $?;
}

installVirtualEnv(){
    cd $BASE/src;
    wget https://pypi.python.org/packages/8b/2c/c0d3e47709d0458816167002e1aa3d64d03bdeb2a9d57c5bd18448fd24cd/virtualenv-15.0.3.tar.gz#md5=a5a061ad8a37d973d27eb197d05d99bf;
    tar xvfz virtualenv-15.0.3.tar.gz;
    cd virtualenv-15.0.3;
    $BASE/.localpython/bin/python setup.py install;
    return $?;
}

createVirtualEnv(){
    # create a virtual env in the directory passed in
    venvDir=$1;
    cd $BASE;
    $BASE/.localpython/bin/virtualenv $venvDir --python=$BASE/.localpython/bin/python;
    cd $BASE/$venvDir;
    source bin/activate;
    pip install requests psutil bs4 pytest mock coverage;
    deactivate;
    return $?;
}

cd $BASE;
mkdir src .localpython;
cd src;

installPython "2.7.12" && ok 'Python installed' || fail 'Cannot install python';
installVirtualEnv && ok 'Virtual env installed' || fail 'Cannot install virtual env';
createVirtualEnv "install.venv" && ok 'Virtual env created' || fail 'Cannot create virtual env';
