#!  /bin/bash

ROOT=$(dirname "${BASH_SOURCE[0]}");
export PYTHONPATH=$ROOT/src:$PYTHONPATH;
cd $ROOT;
python -m pytest
